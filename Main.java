package distancePoint;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Main {
    static List<Point> points = new ArrayList<>();

    public static void main(String[] args) {
        Comparator<Point> comparator = (point1, point2) -> (int) (point1.getDistance() - point2.getDistance());
        Scanner scanner = new Scanner(System.in);
        System.out.println("Set number point");
        int numPoit = scanner.nextInt();
        for (int i = 0; i < numPoit; i++) {
            System.out.println("Podaj nazwe punktu i współrzędne x i y ");
            createPoint();
        }
        points.sort(comparator);
        printArray();
    }

    public static void createPoint() {
        Scanner scanner = new Scanner(System.in);
        Point point = new Point(scanner.nextLine(), scanner.nextInt(), scanner.nextInt());
        points.add(point);
    }

    public static void printArray() {
        for (Point values : points) {
            System.out.println(values.toString());
        }
    }
}

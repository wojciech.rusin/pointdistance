package distancePoint;

public class Point {
    private String namePoint;
    private double x;
    private double y;

    @Override
    public String toString() {
        return "Point{" +
                "name='" + namePoint + '\'' +
                ", x=" + x +
                ", y=" + y +
                '}';
    }

    public Point(String name, double x, double y) {
        this.namePoint = name;
        this.x = x;
        this.y = y;
    }

    public double getDistance() {
        double val = Math.pow(x,2) + Math.pow(y, 2);
        double distance = Math.sqrt(val);
        return distance;
    }
}
